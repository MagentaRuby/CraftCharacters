# CraftCharacters

TODO: Rewrite intro paragraph that I lost because of Gitlab's lack of autosave and leaving confirmation dialog!

## Source Mods

*I might not add all of these (limited time/willpower and all), but the first release version (if any) will at least have the first two mods. The mods will be listed in the order of how likely I am to add features from it.*

* [Mine Little Pony]
    * Author: [Verdana], [KillJoy]
    * Source: [Open](https://github.com/MineLittlePony/MineLittlePony)
    * License: [MIT]
    * **Why?**
        * LiteLoader
        * My mod will be based on this one.
        * GUI will be replaced.
        * Network stuff will be changed. Not sure how I'm going to manage that yet.
        * Ponies and other species in this mod will become a [Genus](#new-features).
        * Ponified Mobs can be enabled/disabled per server/world. It's off by default, but that can be changed too.
* [More Player Models]
    * Author: [Noppes]
    * Source: **TODO**
    * License: [CC BY-NC 3.0]
    * **Why?**
        * Most of the humaniod parts will come from this mod.
        * I may be able to make some of the parts compatible with some other genea, but initially all of them will be exclusive to the humanoid genea.
        * The mob disguise feature will be seperated from the character creation, and will probably just be used with a command.
        * Or I might just remove it.
        * If mob disguise is kept, I'll add NBT support to the command.
* [Tails]
    * Author: [Kihira]
    * Source: **TODO**
    * License: **TODO**
    * **Why?**
        * More tail, ears, and wings options!
        * The color picker.
* [Gender]
    * Author: [iPixeli]
    * Source: **TODO**
    * License: [Custom](ipixeli-gender-license.txt)
    * **Why?**
        * Alternative breast shapes.
        * Alternative sounds.
        * Maybe there's another feature idk about.
* [Lovely Robots]
    * *I don't know who the original author of this mod is. If you are the author or know know who they are please contact me.*
    * **Why?**
        * Because I like the robot model in this mod. Yeah, I know the breast size is rediculous; I can make that optional and add smaller ones.
        * The robot model will be added as a new genus which I haven't decided the name of.

[Mine Little Pony]: http://www.minelittlepony-mod.com/
[Verdana]: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1278090-mine-little-pony-v1-6-4-1
[KillJoy]: https://github.com/killjoy1221

[More Player Models]: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1291730-more-player-models-2-adds-a-character-creation
[Noppes]: http://www.minecraftforum.net/members/Noppes

[Gender]: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1273164-ipixelis-gender-mod
[iPixeli]: http://www.minecraftforum.net/members/iPixeli

[Tails]: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/2189718-1-8-0-tails-a-bunch-of-different-tails-ears-wings
[Kihira]: http://www.minecraftforum.net/members/Kihira

[Lovely Robots]: http://www.9minecraft.net/lovelyrobot-mod/

[MIT]: https://opensource.org/licenses/MIT
[CC BY-NC 3.0]: https://creativecommons.org/licenses/by-nc/3.0/

## New Features

* Universal GUI
    * Replaces "Skin Customisation" in options.
    * Also accessable by an inventory button.
    * The button's location can be changed to avoid interferring with other mods.
* Character Creation
    1. **Genus**: The base player model.
    1. **Species/Gender**: A template with preset parts, colors, and sounds to start from.
    1. **Appearance**: The main screen.
* *Genea*
    * **Humanoid**
        * Based on the vanilla character model.
        * *Species*
            * Human
            * Elf
            * Feline
            * Canine
            * Bunny
            * Naga
            * Centaur
            * Orc
    * **Quadraped**
        * Dependent on Mine Little Pony
        * *Species*
            * Earth Pony
            * Pegasus
            * Unicorn
            * Alicorn
            * Zebra
            * Changeling
    * **Misunoid**
        * Dependent on Lovely Robots
        * *Species*
            * Vanilla
            * Ikaros
            * Bunny Bot
            * (Flat-chested character)
